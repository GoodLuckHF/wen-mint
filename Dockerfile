FROM python:3.10.6-slim-bullseye AS build
LABEL maintainer "The Yellow Dart"

RUN python -m venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"

COPY requirements.txt .
COPY wen_mint ./wen_mint
COPY setup.py .
COPY README.md .
RUN pip install --no-cache-dir wheel
RUN pip install --no-cache-dir -r requirements.txt
RUN pip install --no-cache-dir --no-input .

FROM python:3.10.6-slim-bullseye AS runtime

WORKDIR /usr/app
COPY --from=build /opt/venv /opt/venv

ENV PATH="/opt/venv/bin:$PATH"
ENV PYTHONPATH "${PYTHONPATH}:/opt/venv/bin"
CMD /bin/bash
